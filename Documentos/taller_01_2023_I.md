# Taller 01

**Importante:**
Para resolver los siguientes ejercicios debe crearse una cuenta en la
plataforma [Codingame.](https://www.codingame.com/start)

<br>
<br>

## Ejercicio 1
[TEMPERATURES](https://www.codingame.com/training/easy/temperatures)

### STATEMENT
Your program must analyze records of temperatures to find the closest to zero.

<br>
<br>

## Ejercicio 2
[HORSE-RACING DUALS](https://www.codingame.com/training/easy/horse-racing-duals)

### STATEMENT
In this problem you have to find the two numbers that are closest to each other among a list of numbers. The list might be really large and force you to search for the best possible algorithmic complexity for your solution.

<br>
<br>

## Ejercicio 3
[UNARY](https://www.codingame.com/training/easy/unary)

### STATEMENT
Your program must encode a string into a series of zeros.
A string is an encoded form of digital values and you have to transform those values into a new format.

<br>
<br>

## Ejercicio 4
[PERSONAL BEST](https://www.codingame.com/training/easy/personal-best)

### STATEMENT
The puzzle input provides one or more gymnasts' names and competition categories, for example "Simone Biles" and "bars", following which will be a set of competition records. Each record contains a gymnast's name and the points they scored on the bars, the balance beam, and for floor work. There may be more than one competition record per gymnast.

The puzzle requires that you read through all the records, and find the highest score for each of the given gymnast/s in the given category/categories.

<br>
<br>

## Ejercicio 5
[DOLBEAR'S LAW](https://www.codingame.com/training/easy/dolbears-law)

### STATEMENT
Dolbear's law states the relationship between the air temperature and the rate at which crickets chirp (source: Wikipedia).

Dolbear expressed the relationship as the following formula, which provides a way to estimate the temperature TC in degrees Celsius from the number of chirps per minute, called N60:
TC = 10 + (N60 - 40) / 7.

Another method is to count the number of chirps in 8 seconds, called N8, and add 5
(this is fairly accurate between 5 and 30°C):
TC = N8 + 5.

We are in August and it is very hot.
With the help of Jiminy Cricket, you want to estimate the air temperature while staying cool at home.
You then take a stopwatch, a pencil and a paper, and every 4 seconds, you note the number of chirps of Jiminy.

For example, if you noted the following series: 3 2 3, then Jiminy chirped 3 times in the first 4 seconds, then 2 times in the next 4 seconds, and finally 3 times in the last 4 seconds.

In order to have a good estimate of the air temperature, you take a lot of measurements: one measure every 4 seconds during M minutes.

Once you're done with the measurements, you can compute the estimated temperature using the correct formula!
Unfortunately, Jiminy chirps whenever he wants, driving you crazy and distorting your calculations... so do not take your results too seriously!

<br>
<br>

## Ejercicio 6
[DEFIBRILLATORS](https://www.codingame.com/training/easy/defibrillators)

### STATEMENT
The goal of this exercise is to make you find the closest point to given geographic coordinates (latitude and longitude). Your program must print the associated location name.

In this puzzle you will play a lot with conversion (degree to radian, coordinates to distance, string to float).

<br>
<br>

## Ejercicio 7
[RIVER CROSSING](https://www.codingame.com/training/medium/river-crossing)

### STATEMENT
There is a farmer, a wolf, a goat and a cabbage.
They need to cross a river with the following restrictions:

* The farmer can move from one side to the other freely, and can optionally carry one other entity
* The wolf can’t be on the same side with the goat without the farmer
* The goat can’t stay at the same side with the cabbage without the farmer

The river has two sides L for Left and R for Right.

You are given the initial positions and the target positions, in the following order Farmer, Wolf, Goat, Cabbage. For example you may be given the positions like so

L L L R which would mean that the farmer, wolf, and goat are on the left side and the cabbage is on the right side.

Without breaking the restrictions, print out the minimum legal states starting at the initial state to get to the target state and including the target state.

FOR MULTIPLE SOLUTIONS
If there are multiple solutions with the same length, return the one that is alphabetically first.

Example Input
- L L L R
- L L L L

Desired Output
- L L L R
- R L R R
- L L R L
- R L R L
- L L L L

Explanation
- L L L R // Starting State
- R L R R // Farmer takes goat to right
- L L R L // Farmer takes cabbage left
- R L R L // Farmer returns to goat
- L L L L // Farmer takes goat to left

<br>
<br>

## Ejercicio 8
[DEATH FIRST SEARCH - EPISODE 1](https://www.codingame.com/training/medium/death-first-search-episode-1)

### STATEMENT
This problem plays out on a graph where a “virus” moves from node to node, in search of an exit. There are several exits and you have to cut access to these exits by finding the best link to cut each turn.
