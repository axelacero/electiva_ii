def fibon(n):
    a = 0
    b = 1
    resultado = []

    for i in range(n):
        resultado.append(a)
        a, b = b, a + b

    return resultado


def run():
    resultado = fibon(1000000)
    print(*resultado, sep='\n')


if __name__ == '__main__':
    run()
