num1 = int(input('\nIngrese un número entero: '))
num2 = int(input('Ingrese otro número entero: '))

print(f'\n{num1} es mayor que {num2}\n' if num1 >
      num2 else f'\n{num1} es igual a {num2}\n' if num1 == num2 else f'\n{num1} es menor que {num2}\n')
