pi = 3.14159265358979323846
print(f'{pi} es de tipo {type(pi)}')


ciudad = 'Santa Marta'
print(f'{ciudad} es de tipo {type(ciudad)}')


verdad = True
print(f'{verdad} es de tipo {type(verdad)}')


print('\n')


pi = 3
print(f'{pi} es de tipo {type(pi)}')


ciudad = 2.5
print(f'{ciudad} es de tipo {type(ciudad)}')


verdad = 'False'
print(f'{verdad} es de tipo {type(verdad)}')
