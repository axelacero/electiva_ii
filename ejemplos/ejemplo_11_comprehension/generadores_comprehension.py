def run():
    generador = (x**2 for x in range(10000))

    for i in generador:
        print(i)


if __name__ == '__main__':
    run()
