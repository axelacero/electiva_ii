def escribir_archivo():

    num = [num for num in range(11)]
    num_cuadrado = [num_cuadrado**2 for num_cuadrado in range(11)]
    num_cubo = [num_cubo**3 for num_cubo in range(11)]

    with open('numeros.txt', 'w') as f:
        f.write('    x      x^2    x^3\n')
        for i in range(len(num)):
            f.write('{num:>5}  {num_cuadrado:>5}  {num_cubo:>5}\n'.format(
                num=num[i], num_cuadrado=num_cuadrado[i], num_cubo=num_cubo[i]))


def run():
    escribir_archivo()


if __name__ == '__main__':
    run()
